<?php

use Atlassian\JiraRest\Requests\Middleware\BasicAuthMiddleware;

return [

    'issue_prefix' => env('JIRA_ISSUE_PREFIX', ''),

    'transitions' => [
        'CodeReview'  => env('JIRA_TRANSITION_CodeReview'),
        'Discovery'   => env('JIRA_TRANSITION_Discovery'),
        'ReOpen'      => env('JIRA_TRANSITION_ReOpen'),
        'Development' => env('JIRA_TRANSITION_Development'),
        'Review'      => env('JIRA_TRANSITION_Review'),
    ],

    'host' => env('JIRA_HOST'),

    'default_auth' => env('JIRA_AUTHENTICATION', 'basic'),

    'auth' => [
        'basic' => [
            'username' => env('JIRA_USER'),
            'password' => env('JIRA_PASS'),
        ],

        'oauth' => [
            'consumer_key'           => env('JIRA_CONSUMER_KEY', ''),
            'consumer_secret'        => env('JIRA_CONSUMER_SECRET', ''),
            'oauth_token'            => env('JIRA_OAUTH_TOKEN', ''),
            'oauth_token_secret'     => env('JIRA_OAUTH_TOKEN_SECRET', ''),
            'private_key'            => env('JIRA_PRIVATE_KEY', ''),
            'private_key_passphrase' => env('JIRA_PRIVATE_KEY_PASSPHRASE', ''),
            'impersonate'            => env('JIRA_IMPERSONATE', false),

            'routes' => env('JIRA_OAUTH_ROUTES', false),
        ],
    ],

    'log_level' => env('JIRA_LOG_LEVEL', 'WARNING'),

    'client_options' => [
        'auth' => BasicAuthMiddleware::class,
    ],

    'session'      => [
        'name'     => 'jira_session',
        'duration' => 3600,
    ],

    // List of custom fields defined in Jira
    'customfields' => [],

];
