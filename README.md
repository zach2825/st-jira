# SETUP

- create the database and import task.sql.gz to it

Leave -p empty it will prompt you.sql Just change USERNAME to whatever you use
`mysql -uUSERNAME -p -e"create database task"; zcat task.sql.gz | mysql -uUSERNAME -p task`

- Copy the .env.example file to .env `cp .env{.example,}`

- set the things in the .env file 
Generate the api password key in jiras profile page import set the password in the password section of of the .env file along with the other settings

- make jira-worker executable and symlink to along directory in your $PATH like mine was ~/scripts 

- run jira-worker from the base of the project it should output the list of commands you can do

- enjoy



## Add this - https://github.com/cpliakas/git-wrapper
