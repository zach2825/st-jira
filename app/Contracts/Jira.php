<?php


namespace App\Contracts;


use App\Models\User;
use Atlassian\JiraRest\Exceptions\JiraClientException;
use Atlassian\JiraRest\Exceptions\JiraNotFoundException;
use Atlassian\JiraRest\Exceptions\JiraUnauthorizedException;
use Atlassian\JiraRest\Requests\Issue\Parameters\Comment\AddParameters;
use Exception;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Arr;
use JiraRestApi\Issue\IssueService;
use JiraRestApi\JiraException;
use stdClass;

class Jira
{
    public $CodeReview  = null;
    public $Discovery   = null;
    public $ReOpen      = null;
    public $Development = null;
    public $Review      = null;

    public $type = null;

    public $jql = 'project = ST AND issuetype in (Bug, Hotfix, Improvement, \'New Feature\', Spike, Task, "Tech Debt", Sub-task) AND status = Open ORDER BY Rank ASC';

    /**
     * @var PullTaskNumber
     */
    public $taskNumber;
    public $issue_prefix;
    /**
     * @var Repository|Application|mixed
     */
    public $jira_host;
    /**
     * @var array|stdClass
     */
    protected $found_issue;

    public function __construct(PullTaskNumber $taskNumber)
    {
        if (empty(env('JIRA_HOST')) || empty(env('JIRA_USER')) || empty(env('JIRA_USER_NAME')) || empty(env('JIRA_PASS'))) {
            die('Missing required jira stuff. Probably need to copy .env.example to .env.');
        }

        $this->taskNumber   = $taskNumber;
        $this->issue_prefix = config('atlassian.jira.issue_prefix');
        $this->jira_host    = config('atlassian.jira.host');

        $this->CodeReview  = config('atlassian.jira.transitions.CodeReview');
        $this->Discovery   = config('atlassian.jira.transitions.Discovery');
        $this->ReOpen      = config('atlassian.jira.transitions.ReOpen');
        $this->Development = config('atlassian.jira.transitions.Development');
        $this->Review      = config('atlassian.jira.transitions.Review');
    }

    public function setCommandOptions($task_id = null, $jql = null)
    {
        if (empty($jql)) {
            $jql = 'project = ST AND issuetype in (Bug, Hotfix, Improvement, \'New Feature\', Spike, Task, "Tech Debt", Sub-task) AND status = Open ORDER BY Rank ASC';
        }

        $this->taskNumber->setCommandOptions($task_id);
        $this->jql = $jql;

        return $this;
    }

    public function setTaskID($task_id)
    {
        $this->taskNumber->task_id = $task_id;

        return $this;
    }

    public function doTransition(int $transition_id)
    {
        $rtv = false;
        try {
            jira()->issue($this->taskNumber->pullTaskKey())->doTransition($transition_id);

            return true;
        } catch (JiraClientException $e) {
            dd($e->getMessage());
        } catch (Exception $e) {
            dd($e->getMessage());
        }

        return $rtv;
    }

    public function getTransitions()
    {
        return jira()->issue($this->taskNumber->pullTaskKey())->getTransitions();
    }

    public function assignMe()
    {
        return jira()->issue($this->taskNumber->pullTaskKey())->edit(['fields' => ['assignee' => ['name' => env('JIRA_USER_NAME')]]]);
    }

    /**
     * @param               $task_id
     * @param AddParameters $comment
     * @return array|stdClass|string
     */
    public function issueAddComment($task_id, AddParameters $comment)
    {
        try {
            return jira()->issue($task_id)->addComment($comment);
        } catch (JiraNotFoundException $e) {
            return 'Jira no issue: ' . $e->getMessage() . PHP_EOL;
        } catch (JiraUnauthorizedException $e) {
            dd('bad_auth');
        } catch (JiraClientException $e) {
            return 'General jira error: ' . $e->getMessage() . PHP_EOL;
        }

        return 'error';
    }

    public function getIssue()
    {
        try {
            $found_issue = jira()->issue($this->taskNumber->pullTaskKey())->get();

            if (!$found_issue) {
                die('nothing returned');
            }

            $this->found_issue = $found_issue;

            return $found_issue;

        } catch (JiraNotFoundException $e) {
            die(json_encode([
                'status' => false,
                'error'  => $e->getMessage(),
            ]));
        }
    }

    public function logTask()
    {
        $found_issue = $this->found_issue;

        $user = User::first();

        $_task = $user->tasks()->updateOrCreate(['task_id' => $this->taskNumber->task_id], [
            'title'       => str_limit($found_issue['fields']['summary'], 250),
            'description' => str_limit(Arr::get($found_issue, 'fields.description', ''), 250),
            'task_branch' => $this->getBranchName(), // Build new branch name
            'task_id'     => $this->taskNumber->task_id,
            'jira_type'   => array_get($this->found_issue, 'fields.issuetype.name', 'Task'),
        ]);

        return $_task;
    }

    public function getBranchName()
    {
        $found_issue   = $this->found_issue;
        $type          = $this->mapJiraType();
        $branch_prefix = config('branching.branch_prefix');

        // Build new branch name
        return sprintf(
            '%s/%s/%s',
            $type,          // task type bug or feature usually
            // sluggable description
            preg_replace('/-$/', '', str_limit(str_slug("{$this->pullTaskKey()} {$found_issue['fields']['summary']}"), 22, '')),
            $branch_prefix // github username
        );
    }

    public function mapJiraType()
    {
        if ($this->type) {
            return $this->type;
        }

        $type = array_get($this->found_issue, 'fields.issuetype.name');

        switch (strtolower($type)) {
            case "bug":
                return 'bug';

            case "task":
            case "subtask":
            default:
                return 'feature';
        }
    }

    public function pullTaskKey()
    {
        return array_get($this->found_issue, 'key');
    }

    public function getIssues()
    {
        try {
            $issueService = new IssueService();
            $list         = $issueService->search($this->jql);

            if (!count($list->issues)) {
                die("The jql returned no results.\r\n{$this->jql}\r\n");
            }

            return $list->issues;
        } catch (JiraException $e) {
            dd($e->getMessage());
        } catch (Exception $e) {
            dd($e->getMessage());
        }

        return null;
    }
}
