<?php

namespace App\Commands;

use App\Contracts\GitStuff;
use App\Models\Tasks;
use LaravelZero\Framework\Commands\Command;

class ListTasksCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'tasks {task_id? : task to show }
                                {--l|last : List the last task created}
                                {--history= : How many items to show}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'List tasks';

    /**
     * Execute the console command.
     *
     * @param GitStuff $gitStuff
     * @return mixed
     */
    public function handle(GitStuff $gitStuff)
    {
        $task = null;

        if (!$this->argument('task_id') || $this->option('last')) {
            $task = Tasks::orderBy('id', 'desc')->first();
        } elseif ($this->argument('task_id')) {
            $task = Tasks::find($this->argument('task_id'));
        }

        if ($this->option('history')) {
            $tasks = Tasks::orderBy('id', 'desc')->limit($this->option('history'))->get();
            $rows  = [];

            foreach ($tasks as $inc => $task) {

                $this->table(['Command', 'Output'],
                    [
                        ['Git subject', "ST-{$task->task_id} - {$task->title}"],
                        ['PR Link', "https://github.com/OpenTechAlliance/st-react/compare/develop...{$gitStuff->branchPrefix}/{$task->task_branch}?expand=1"],
                        ['Task Link', "https://opentechlabs.atlassian.net/browse/ST-{$task->task_id}"],
                        ['---', '---'],
                        ['Task ID', "ST-{$task->task_id}"],
                        ['Subject', $task->title],
                        ['Branch', "{$gitStuff->branchPrefix}/{$task->task_branch}"],
                        ['Description', trim($task->description)],
                        ['Log Created At', trim($task->created_at)],
                    ]
                );
            }

            return 0;
        }

        if (!$task) {
            $this->error('Task has not been logged yet');

            return false;
        }

        $this->table(['Command', 'Output'],
            [
                ['Git subject', "ST-{$task->task_id} - {$task->title}"],
                ['PR Link', "https://github.com/OpenTechAlliance/st-react/compare/develop...{$gitStuff->branchPrefix}/{$task->task_branch}?expand=1"],
                ['Task Link', "https://opentechlabs.atlassian.net/browse/ST-{$task->task_id}"],
                ['---', '---'],
                ['Task ID', "ST-{$task->task_id}"],
                ['Subject', $task->title],
                ['Branch', "{$gitStuff->branchPrefix}/{$task->task_branch}"],
                ['Description', trim($task->description)],
                ['Log Created At', trim($task->created_at)],
            ]
        );

        $this->error('Nothing to output');

        return false;
    }
}
