<?php

namespace App\Commands;

use App\Contracts\PullTaskNumber;
use LaravelZero\Framework\Commands\Command;

class StartWorkCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'start-work';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Basically mark the task as development';

    /**
     * Execute the console command.
     *
     * @param PullTaskNumber $taskNumber
     * @return mixed
     */
    public function handle(PullTaskNumber $taskNumber)
    {
        $taskNumber->setCommandOptions(null, true);

        $this->call('get:i', [
            'task_id'       => $taskNumber->pullTaskID(),
            '--development' => 'default',
            '--only'        => 'default',
        ]);

        return 0;
    }
}
