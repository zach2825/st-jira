<?php

namespace App\Commands;

use App\Contracts\GitStuff;
use App\Contracts\PullTaskNumber;
use LaravelZero\Framework\Commands\Command;

//use GrahamCampbell\GitHub\Facades\GitHub;

class MakePrCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'make-pr {task_id? : Task ID I can try to get it from the branch}
                                {--s|service=github : which git provider}
                                {--bf|branch-from= : which branch to merge into}
                                {--bt|branch-to=develop : which branch to merge into}

                                {--p|in-progress : Will prepend [WIP] to the title}
                                {--list-prs : dump all PRs from the repo. warning this is a lot of information}

                                {--d|description= : Describe what was done, replace newlines with \r\n}

                                {--no-push : don\'t push up the code}

                                {--mark-pr : Update the task in jira and set it to in progress}
                                {--add-jira-comment=0 : will prompt for a simple message}
                            ';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Push up code and create the PR';

    /**
     * Execute the console command.
     *
     * @param PullTaskNumber $pullTaskNumber
     * @param GitStuff       $gitStuff
     * @return mixed
     */
    public function handle(PullTaskNumber $pullTaskNumber, GitStuff $gitStuff)
    {
        if ($this->option('list-prs')) {
            $prs = $gitStuff->getPRS();

            dd($prs);
        }

        // Track the task id
        $pullTaskNumber->setCommandOptions($this->argument('task_id'), true);

        // update remote
        if ($pullTaskNumber->branch && !$this->option('no-push')) {
            $gitStuff->push($pullTaskNumber->branch);
        }

        // Make the PR and open the browser
        $output = null;
        exec("gh pr create -w -b {$this->option('branch-to')}");


        $move_task_to_pr = false;

        // Check to see if we want to mark this Task PR in jira
        if (!$this->option('mark-pr')) {
            $answer = $this->confirm('Would you like to mark this task "Code review"?', 'y');

            $move_task_to_pr = (stripos(strtolower($answer), 'y') === 0);
        }

        // Mark the task in jira code review
        if ($move_task_to_pr || $this->option('mark-pr')) {

            $this->call('get:i', [
                'task_id'  => $pullTaskNumber->pullTaskID(),
                '--review' => 'default',
            ]);
        }

        $this->info('Collecting the branch commit messages');
        $messages = $gitStuff->getBranchMessages();
        $message  = $gitStuff->formatLogMessage($messages);

        if (strtolower($this->confirm('Add this message to the task in jira? ' . $message, 'n')) === true) {
            $pullTaskNumber->addTaskComment($messages);

            $this->info('The comment has been tracked.');
        }

        return true;
    }
}
