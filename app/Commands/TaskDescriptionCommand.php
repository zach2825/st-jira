<?php

namespace App\Commands;

use App\Contracts\GitStuff;
use App\Contracts\Jira;
use App\Contracts\PullTaskNumber;
use LaravelZero\Framework\Commands\Command;

class TaskDescriptionCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'desc {task_id? : Task number} {--b|branch=develop : Which branch to merge to}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Show the task description';

    /**
     * Execute the console command.
     *
     * @param PullTaskNumber $pullTaskNumber
     * @param GitStuff       $gitStuff
     * @param Jira           $jira
     * @return mixed
     */
    public function handle(PullTaskNumber $pullTaskNumber, GitStuff $gitStuff, Jira $jira)
    {
        $pullTaskNumber->setCommandOptions($this->argument('task_id'), true);

        $task_id = $pullTaskNumber->pullTaskID();
        $task    = $pullTaskNumber->getTask();

        $branch_prefix = config('branching.branch_prefix');

        $this->table(['Command', 'Output'],
            [
                ['Git subject', "ST-{$task->task_id} - {$task->title}"],
                ['PR Link', "https://github.com/{$gitStuff->organization}/$gitStuff->repo/compare/{$this->option('branch')}...{$branch_prefix}/{$task->task_branch}?expand=1"],
                ['Task Link', "{$jira->jira_host}/browse/{$jira->issue_prefix}{$task_id}"],
                ['---', '---'],
                ['Task ID', "{$jira->issue_prefix}{$task->task_id}"],
                ['Branch', $task->task_branch],
                ['Description', trim($task->description)],
                ['Log Created At', trim($task->created_at)],
            ]
        );

        return true;
    }
}
