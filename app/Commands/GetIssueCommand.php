<?php

namespace App\Commands;

use App\Contracts\GitStuff;
use App\Contracts\Jira;
use App\Contracts\PullTaskNumber;
use App\Models\Tasks;
use LaravelZero\Framework\Commands\Command;

class GetIssueCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'get:info {task_id : Just the id of the issue minus the ST- part}
                                        {--a|assign-me : assign me to the task}
                                        {--no-branch : don\'t create the branch}
                                        {--t|transitions : dump available transitions}

                                        {--i|info : dump what comes back from jira}
                                        {--dump : dump subject and description}

                                        {--discovery : try to set this issues status to discovery}
                                        {--d|development : try to set this issues status to development}
                                        {--re-open : try to set this issues status to development}
                                        {--r|review : try to set this issues status to Code Review}

                                        {--switch-dev : Don\'t  switch back to develop before creating/ switching to the new branch}
                                        {--1|only : Only do one thing if passed in}

                                        {--type= : What git branch prefix to use for example feature or bug. Will prepend the generated branch name with feature/key-description}
                            ';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Look in jira and get the issue subject and description';

    /**
     * Execute the console command.
     *
     * @param PullTaskNumber $taskNumber
     * @param Jira           $jira
     * @param GitStuff       $gitStuff
     * @return mixed
     */
    public function handle(Jira $jira, GitStuff $gitStuff)
    {
        if (str_contains(strtolower($this->argument('task_id')), 'st-')) {
            $this->warn('Without the "st-" please');

            die();
        }

        if ($this->option('transitions')) {
            $this->info(print_r($jira->getTransitions(), true));

            return 0;
        }

        // Track task id
        $jira->setCommandOptions($this->argument('task_id'));

        if ($this->option('type')) {
            $jira->type = $this->option('type');
        }

        $this->task('Getting task details', function () use ($jira) {
            $found_issue = $jira->getIssue();

            if ($this->option('info')) {
                dd(json_encode($found_issue));
            }

            if ($this->option('dump')) {
                dd([
                    'title'       => str_limit($found_issue['fields']['summary'], 250),
                    'description' => str_limit($found_issue['fields']['description'], 250),
                ]);
            }

            $jira->logTask();

            return true;
        });

        /** @var Tasks $task */
        $task = $jira->taskNumber->getTask();

        if (is_string($task)) {
            $this->info($task);

            return 1;
        }

        if ($this->option('discovery')) {
            $this->info("Review link: {$jira->issue_prefix}/browse/" . $jira->taskNumber->pullTaskKey());

            $this->task(sprintf('Change this task %s to Discovery', $jira->taskNumber->pullTaskID()), function () use ($jira) {
                return $jira->doTransition($jira->Discovery);
            });

            if ($this->option('only')) {
                return true;
            }
        }

        if ($this->option('re-open')) {
            $this->task(sprintf('Change this task Back %s to Development', $jira->taskNumber->pullTaskID()), function () use ($jira) {
                return $jira->doTransition($jira->ReOpen);
            });

            if ($this->option('only')) {
                return true;
            }
        }

        if ($this->option('development')) {
            $this->task(sprintf('Change this task %s to Development', $jira->taskNumber->pullTaskID()), function () use ($jira) {
                return $jira->doTransition($jira->Development);
            });

            if ($this->option('only')) {
                return true;
            }
        }

        if ($this->option('review')) {
            $this->task(sprintf('Change this task %s to Code Review', $jira->taskNumber->pullTaskID()), function () use ($jira) {
                return $jira->doTransition($jira->CodeReview);
            });

            if ($this->option('only')) {
                return true;
            }
        }

        if ($this->option('assign-me') && env('JIRA_USER_NAME')) {
            $this->task(sprintf('Assigning %s to %s', env('JIRA_USER_NAME'), $jira->taskNumber->pullTaskID()), function () use ($jira) {
                return $jira->assignMe();
            });

            if ($this->option('only')) {
                return true;
            }
        }

        if (!$this->option('no-branch')) {
            $task_branch = $task->task_branch;

            if ($this->option('switch-dev')) {
                $this->info('Checking out to the develop branch');
                $gitStuff->checkoutDevelop();
            }

            $this->task('Generating or checking out to ' . $task_branch, fn() => $gitStuff->makeBranch($task_branch));
        }

        return 0;
    }
}
