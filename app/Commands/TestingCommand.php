<?php


namespace App\Commands;


use App\Contracts\GitStuff;
use App\Contracts\PullTaskNumber;
use Illuminate\Console\Command;

class TestingCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'test:testing {task_id?}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Just a random space to test command stuff';

    public function handle(PullTaskNumber $pullTaskNumber, GitStuff $gitStuff)
    {
        $pullTaskNumber->setCommandOptions($this->argument('task_id'), true);

        $this->info('Collecting the branch commit messages');
        $messages = $gitStuff->getBranchMessages();
        $message  = $gitStuff->formatLogMessage($messages);

        if (strtolower($this->confirm('Add this message to the task in jira? ' . $message, 'n')) === true) {
            $pullTaskNumber->addTaskComment($messages);

            $this->info('The comment has been tracked.');
        }
    }
}
