<?php /** @noinspection ALL */

namespace App\Commands;

use App\Contracts\Jira;
use LaravelZero\Framework\Commands\Command;

class JiraNextInLineCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'jira:next-in-line
                                {--jql=  : Search string to get next in line}
                                {--task_id= : Force the process to run for a specific task_id}
                                {--show_task=3 : how many tasks to list in the menu}
                                ';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Get the next in line issue';

    /**
     * Execute the console command.
     *
     * @param Jira $jira
     * @return mixed
     */
    public function handle(Jira $jira)
    {
        // No task id was passed in show a menu so one can be picked.
        if (!$this->option('task_id')) {
            $jira->setCommandOptions(null, $this->option('jql'));

            $issues = $jira->getIssues();

            $rows = array_map(function ($issue, $key) {
                if ($key >= $this->option('show_task')) {
                    return false;
                }

                return sprintf("%sKey: %s\r\nSelf: %s\r\nSummary: %s\r\nDescription: %s\r\n", ($key === 0) ? 'Next in line - ' : '',
                    object_get($issue, 'key'),
                    object_get($issue, 'self'),
                    str_limit(object_get($issue, 'fields.summary', ''), 50),
                    str_limit(object_get($issue, 'fields.description', ''), 100)
                );
            }, $issues, array_keys($issues));

            // Clean up the array
            $rows = array_filter($rows);

            $selected_row = $this->menu('Select task', $rows)
                                 ->setWidth(200)
                                 ->setBorder(1, 1, 'green')
                                 ->setPadding(1)
                                 ->addLineBreak('*', 1)
                                 ->open();

            if ($selected_row === null || $selected_row < 0) {
                $this->info('Bye');

                return true;
            }

            $answer = $this->confirm("Start issue {$issues[$selected_row]->key}", 'y');

            if (stripos(strtolower($answer), 'y') === 0) {
                $key = str_replace($jira->issue_prefix, '', $issues[$selected_row]->key);

                // checkout the new task, move it to discovery, create the new branch, and log in the database when it was created locally.
                $this->call('get:i', [
                    'task_id'     => $key,
                    '--assign-me' => 'default',
                    '--discovery' => 'default',
                ]);
            }
            $this->info('Success');

            return true;
        }


        // The task id option was passed in.

        $answer = $this->confirm("Start issue {$jira->issue_prefix}{$this->option('task_id')}", 'y');

        if (stripos(strtolower($answer), 'y') === 0) {

            // checkout the new task, move it to discovery, create the new branch, and log in the database when it was created locally.
            $this->call('get:i', [
                'task_id'     => $this->option('task_id'),
                '--assign-me' => 'default',
                '--discovery' => 'default',
            ]);
        }
        $this->info('Success');

        return true;
    }
}
