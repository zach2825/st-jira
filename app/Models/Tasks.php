<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property User   user
 * @property string task_id
 * @property string description
 * @property string task_branch
 * @property string title
 * @property Carbon created_at
 */
class Tasks extends Model
{
    use SoftDeletes;

    protected $fillable
        = [
            'title',
            'description',
            'user_id',
            'task_branch',
            'task_id',
            'jira_type',
        ];

    public static function task_id($task_id): Tasks
    {
        return self::where('task_id', $task_id)->firstOrFail();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
