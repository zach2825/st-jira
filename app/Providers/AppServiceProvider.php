<?php

namespace App\Providers;

use App\Contracts\GitStuff;
use App\Contracts\PullTaskNumber;
use Illuminate\Support\ServiceProvider;
use Phar;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(['logging.channels.single.path' => Phar::running()
            ? dirname(Phar::running(false)) . 'output.log'
            : storage_path('laravel.log'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PullTaskNumber::class, function () {
            return new PullTaskNumber();
        });

        $this->app->singleton(GitStuff::class, function ($app) {
            return new GitStuff($app[PullTaskNumber::class]);
        });
    }
}
